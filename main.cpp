/*
Name: John McDonnell.
Student Number: D00096987.
Course: Computing Level 8.
Year: 2.
Group: 2.
Module: Data Structures Ca 1.
Due Date: 4th March 2015.
Version: Final
*/
#include <string>
#include <fstream>
#include <iostream>
#include "OrderedArray.h"
#include <sstream>
using namespace std;

int main(int argc, char **argv)
{
	// creating an array
	OrderedArray<int> array(1);

	string line;
	//reading in file
	ifstream myfile("inputFile.txt");

	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			//cout << line << '\n';
			array.push(stoi(line));
			// pushing each value into array
			// sorting in push function
		}
		// close input file
		myfile.close();
	}
	else
	{
		cout << "Unable to open file";
	}

	//open new file
	ofstream outFile("outputFile.txt");

	for (int x = 0; x < array.length(); x++)
	{
		//after sort put each element into the final array.
		outFile << array.getElement(x) << endl;
	}

	//close outputFile
	outFile.close();

	//arr.display();
	//cout << "Display" << endl;
	//cout << array.search(6);
	//cout << "Remove index 3" << endl;
	//array.remove(3);

	return 0;
};
