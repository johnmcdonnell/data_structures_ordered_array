/*
Name: John McDonnell.
Student Number: D00096987.
Course: Computing Level 8.
Year: 2.
Group: 2.
Module: Data Structures Ca 1.
Due Date: 4th March 2015.
Version: Final
*/

#ifndef ORDEREDARRAY_H_
#define ORDEREDARRAY_H_
using namespace std;
#include <string>

// template class
template <class T>

class OrderedArray
{

private:
	// variables
	T *m_Array;
	int m_Current_Size;
	int m_Growth;
	int m_Grow_Size_Final;

public:
	//constructor passing in grow size
	// array will grow if needed
	OrderedArray(int grow_size)
	{

		m_Growth = grow_size;
		m_Grow_Size_Final = m_Current_Size + grow_size; //10
		m_Array = new T[m_Grow_Size_Final];
		m_Current_Size = 0;

	}
	// default constructor
	OrderedArray()
	{
		m_Array = new T[10]; //10
		m_Current_Size = 0;
		m_Growth = 2;
		m_Grow_Size_Final = grow_size;
	}
	// deconstructor
	~OrderedArray()
	{
		//cout << "test" << endl;
		delete[] m_Array;
		m_Array = 0;
	}

	//push function
	
	void push(const T& newElement)
	{
		// need to re-size ?
		//cout << "in" << endl;

		//if arrays match then carry on
		if (m_Current_Size == m_Grow_Size_Final)
		{

			//cout << "test" << endl;	
			T *temp_Array = m_Array; // pointer to temp array
			m_Array = new T[m_Current_Size + m_Growth]; // create the new array size

			int x = 0;
			while (x < m_Current_Size)
			{
				m_Array[x] = temp_Array[x]; // swap 
				x++;
			}

			m_Array[m_Current_Size] = newElement;

			m_Current_Size++;

			m_Grow_Size_Final++;

			delete[] temp_Array; // delete the array from memory
		}

		else
		{
			m_Array[m_Current_Size] = newElement; // get element in current array

			m_Current_Size++;

		}

		// sort 
		if (m_Current_Size != 1)
		{
			//cout << "test" << endl;
			T temp_Array_Array_1; // two temp arrays
			T temp_Array_Array_2;
			for (int x = 0; x < m_Current_Size - 1; x++)
			{

				for (int x = 0; x< m_Current_Size - 1; x++)
				{

					if (m_Array[x] > m_Array[x + 1]) // check if indexs are different
					{

						//cout << "test" << endl;
						temp_Array_Array_1 = m_Array[x]; // swap

						//cout << temp_Array_Array_1 << endl;
						temp_Array_Array_2 = m_Array[x + 1]; // swap

						m_Array[x + 1] = temp_Array_Array_1; //swap back
						m_Array[x] = temp_Array_Array_2; // swap back

					}
				}
			}
		}

	}
	int search(const T& target) //searches for a specific target value in the array
	{
		int index = 0;

		bool found = false;

		for (int i = 0; i < m_Current_Size; i++)
		{
			if (m_Array[i] == target)
			{
				//cout << "test" << endl;
				found = true;
				index = i;
				break;
			}
			index = -1;
		}
		return index;
	}

	int length()
	{

		return m_Current_Size; //return the current array size

	}

	T getElement(int index) // returns a copy of the element corresponding to the given index.
	{
		T copy_index;
		if (index < m_Current_Size)
		{
			copy_index = m_Array[index];
			return copy_index;
		}
		else
		{
			return -1;
		}
	}

	bool remove(int index) 		//takes one argument � the index of the element you want to remove and removes
		//that element from the array
	{
		int j = 0;
		if (m_Current_Size >= m_Array[index])
		{
			while (j < m_Current_Size)
			{
				m_Array[j] = m_Array[j + 1];
				j++;
				//cout << m_Array[j] << endl;
			}

			m_Current_Size--;

			return true;
		}
		else
		{
			return false;
		}
	}


	void display() // used to test program was sorting and pushing
	{
		for (int i = 0; i < m_Current_Size; i++)
		{
			cout << "i: " << i << " " << m_Array[i] << endl;
		}
	}

	void clear() // clears arrays.
	{
		delete[] m_Array;
		m_Array = 0;
	}

};

#endif /* OrderedArray_H_ */  